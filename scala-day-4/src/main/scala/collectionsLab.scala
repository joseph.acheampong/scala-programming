

object collectionsLab {
  def main(args: Array[String]): Unit = {
    val s = Seq(1,2,3,4,5,6,7)

    val remove = (coll: Seq[Any], idx: Int) => {
      coll.take(idx) ++ coll.drop(idx + 1)
    }

    println(s)
    println(remove(s,1))
    println(remove(s, 3))
  }
}
