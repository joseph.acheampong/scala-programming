def factorialBad(n:BigInt): BigInt = {
  if(n==1) 1
  else n * factorialBad(n-1)
}


def factorial(n: BigInt): BigInt = {
  def factorialAcc(n: BigInt, acc: BigInt): BigInt = {
    if (n <= 1) acc // when n reaches a base case
    else factorialAcc(n - 1, n * acc)
  }
  factorialAcc(n, 1) : BigInt
}

//factorial(5)


// fibonacci
def fib(n:BigInt): BigInt = {
  if(n <= 1) 1
  else fib(n-1) + fib(n-2)
} // horrible because it returns the main function


def fibTail(n: BigInt): BigInt = {
  def fibAcc(n: BigInt, prev: BigInt, current: BigInt): BigInt = {
    if(n <= 1) current
    else fibAcc(n-1, current, prev)
  }

  fibAcc(n, 1, 1)
}

fibTail(1000000)