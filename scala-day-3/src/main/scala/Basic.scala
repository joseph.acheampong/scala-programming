// Write a function that takes as its argument a String
// value describing an operation to be carried out on a pair of integers. For example, add, subtract, power
// Return a function that will perform the required operation on two Ints, returning an Int result.

import scala.math.pow

object Basic {
  def mathWord(operation: String): Int = {
    val add = (x: Int, y: Int) => x + y
    val subtract = (x: Int, y: Int) => x - y
    val power = (x: Int, y:Int) => pow(x, y).toInt

    operation match {
      case "add" => add(1, 2)
      case "subtract" => subtract(2, 1)
      case "power" => power(3, 5)
    }
  }

  def main(args: Array[String]): Unit ={
    println(mathWord("add"))
    println(mathWord("subtract"))
    println(mathWord("power"))
  }
}
