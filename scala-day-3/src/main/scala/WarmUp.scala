object WarmUp{

  val value = new Function5[String, String, String, String, String, Int] {
    override def apply(v1: String, v2: String, v3: String, v4: String, v5: String): Int = s"$v1$v2$v3$v4$v5".length
  }

  val value5: (String, String, String, String, String) => Int = (v1, v2, v3, v4, v5) => s"$v1$v2$v3$v4$v5".length



  def main(args:Array[String]) : Unit = {

    println(value("Joe", "Mills", "Max", "Kezia", "Rocks"))
    println(value5("Joe", "Mills", "Max", "Kezia", "Rocks"))
  }
}
