abstract class Headache {

  val add = (x: Int) => x + 2
  val rand = (x: Any) => 2
  val another = (x: String) => x.length

  val third = (x: Int, y: Any) => 2
  val fourth = (x: String, y: Any) => add(x.length)



  val function: (String, (Int, (String => Int)) => Int) => (Int => Int) =  (aString, aFunction) => add


}
