class Transaction(ID: Int, Symbol: String , Quantity : Int, _price : Double)
  extends Equity(ID, Symbol , Quantity , _price) with FeePayable with Taxable {

  def amount = value + fee + (tax * (value+ fee))

}
