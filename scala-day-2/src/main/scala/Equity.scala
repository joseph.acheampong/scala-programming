class Equity(ID: Int,  Symbol : String,  Quantity: Int, _price : Double  ) extends Trade (ID, _price  ) {
  def isExecutable : Boolean = true

  override def toString = s"$ID, $Symbol, $Quantity, Equity Trade "
  def value: Double = Quantity * price

}
