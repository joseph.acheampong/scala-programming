abstract class Trade (val ID: Int,  private var _price : Double ){
  def isExecutable : Boolean
  // Shorter than java
  override def toString = s"Trade($ID, $Symbol, $price )"
  def price: Double = _price

  def price_= (v : Double) : Unit =
  {
    if (v  >= 0 )_price = v
  }


}
//Companion Object
object Trade {
  //val T2 = new Trade(5,15.25)
  //def apply() = new Trade(5,15.25)
}

case class TradeCase (ID: Int,  Symbol : String, quantity: Int,  private var _price : Double ){
  //def apply() = new Trade(5,"AAPL",100,15.25)

}