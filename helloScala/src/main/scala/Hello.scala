
class Person(val fName:String, val lName:String, private var a:Int){
  // constructor overloading
  def this(f:String, l:String) = this(f, l, 18)

  // getter
  def age = a

  //setter
  def age_=(newAge: Int) = if(newAge > 0 && newAge < 100) a = newAge

}


object Hello {
  def main(args: Array[String]) = {
    println("Hello World")
    val name = "Joseph"
    println(s"My name is $name")
    println(myMax(2,3))

    val human = new Person("Joe", "Test", 24)
    println(s"First Name: ${human.fName} Last Name: ${human.lName} Age: ${human.age}")

    println(s"The temperature in 64.8 degrees celsius is ${convertToFahrenheit(64.8)} F")

    println(convertDateFormat("31/02/15"))
  }


  def myMax(x:Int, y:Int) = if (x>y) x else y

  def convertToFahrenheit(tempInCelsius: Float) = (tempInCelsius * 9/5) + 32


  def convertDateFormat(date: String): String = {
    val splitDate: Array[String] = date.split("/")

    val day = splitDate(0)
    val monthInInt = splitDate(1).toInt
    val year = splitDate(2)

    // add the the th
    var dayTH: String = "th"
    if(day.toInt < 4 || day.toInt > 20){
      if day.endsWith("1") then dayTH = "st"
      else if day.endsWith("2") then dayTH = "nd"
      else if day.endsWith("3") then dayTH = "rd"
    }

    val month = getMonthName(monthInInt)

    val newDate: String = s"${day.toInt}$dayTH $month 20$year"

    newDate
  }


  def getMonthName(month: Int): String = {
    month match {
      case 1 => "January"
      case 2 => "February"
      case 3 => "March"
      case 4 => "April"
      case 5 => "May"
      case 6 => "June"
      case 7 => "July"
      case 8 => "August"
      case 9 => "September"
      case 10 => "October"
      case 11 => "November"
      case 12 => "December"

    }
  }


}